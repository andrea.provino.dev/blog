
const sitemapHandler = {
    pattern: "/sitemap/",
    func: (props) => {
        props.state.source.data["/sitemap/"].isSitemap = true;
        props.state.source.data["/sitemap/"].isArchive = false;
        props.state.source.data["/sitemap/"].isError = false;
    }
}

export {
    sitemapHandler
}