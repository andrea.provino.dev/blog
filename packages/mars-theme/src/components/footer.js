import React from "react";
import { styled, connect } from "frontity";
import Link from "./link";

const Container = styled.div`
  margin-top: 48px;
  padding: 16px 32px;
  background-color: #1c193c;
  color: white;
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const MenuLink = styled(Link)`
  width: 100%;
  display: inline-block;
  outline: 0;
  font-size: 20px;
  margin-left: 8px;
  padding: 4px 8px;
  &:hover,
  &:focus {
    color: #00a8b6;
  }
  /* styles for active link */
  &[aria-current="page"] {
    color: white;
    font-weight: bold;
    /* border-bottom: 4px solid yellow; */
  }
`;

const Title = styled.h2``;
const Section = styled.div`
  flex: 1;
`;
const Footer = ({ state }) => {
  const { menu } = state.theme;
  const isThereLinks = menu != null && menu.length > 0;

  return (
    <Container>
      <Section>
        <Title>Contatti</Title>
        <a
          target="_blank"
          href={"https://it.linkedin.com/in/andrea-provino-638847179"}
        >
          » Andrea Provino
        </a>
        <p>» andrea.provino.dev[@]gmail.com</p>
        <p>» AI BLOG - © 2019-2021</p>
      </Section>
      <Section>
        <Title>Categorie</Title>
        {isThereLinks &&
          menu.map(([name, link]) => (
            <MenuLink
              key={name}
              link={link}
              aria-current={state.router.link === link ? "page" : undefined}
            >
              {name}
            </MenuLink>
          ))}
      </Section>
      <Section></Section>
    </Container>
  );
};

export default connect(Footer);
