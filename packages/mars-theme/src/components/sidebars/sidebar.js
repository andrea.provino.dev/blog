import React from 'react'
import Link from '../link'
import { connect, styled } from "frontity";

const Title = styled(Link)`
margin: 4px 0 2px;
font-weight: 600;
&:hover{
    color: #00a8b6;

}
/* border-bottom: 1px solid #1c193c; */
padding: 8px 0px;
display: block;
`

const Sidebar = ({ state }) => {
    // Get the data of the current list.
    const data = state.source.get(state.router.link);
    // console.log(state)
    return (
        <div>
            <h2>Link rapidi</h2>
            {data.items.map(({ type, id }) => {
                const item = state.source[type][id];
                // Render one Item component for each one.
                return <Title link={item.link} key={item.id} dangerouslySetInnerHTML={{ __html: item.title.rendered }} />;
            })}
        </div>

    )
}


export default connect(Sidebar)