import React, { useEffect } from "react";
import { connect, styled, css } from "frontity";
import Link from "../link";

const Text = styled.em(({ highlight }) => {
  return css`
  display: inline-block;
  margin-top: 16px;
  ${highlight && css`color: #00a8b6; font-weight: 600;`}
`;
})

const Wrapper = styled.div`
display: flex;
justify-content: space-around;
`

/**
 * Pagination Component
 *
 * It's used to allow the user to paginate between a list of posts.
 *
 * The `state`, `actions`, `libraries` props are provided by the global context,
 * when we wrap this component in `connect(...)`
 */
const Pagination = ({ state, actions }) => {
  // Get the total posts to be displayed based for the current link
  const { next, previous } = state.source.get(state.router.link);

  // Pre-fetch the the next page if it hasn't been fetched yet.
  useEffect(() => {
    if (next) actions.source.fetch(next);
  }, []);

  return (
    <Wrapper>
      {/* If there's a next page, render this link */}
      {next && (
        <Link link={next}>
          <Text highlight>← Post Precedenti</Text>
        </Link>
      )}

      {/* If there's a previous page, render this link */}
      {previous && (
        <Link link={previous}>
          <Text >Post Successivi →</Text>
        </Link>
      )}
    </Wrapper>
  );
};

/**
 * Connect Pagination to global context to give it access to
 * `state`, `actions`, `libraries` via props
 */
export default connect(Pagination);


