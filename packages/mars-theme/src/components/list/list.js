import React from "react";
import { connect, styled, decode } from "frontity";
import Item from "./customCard";
import Pagination from "./pagination";
import Sidebar from "../sidebars/sidebar";

const ItemWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  max-width: 600px;
  margin: auto;
`;

const Container = styled.section`
  /* width: 600px; */
  margin: 0;
  margin: 24px;
  list-style: none;
  @media (max-width: 768px) {
    padding: 0px;
  }
`;

const Header = styled.h3`
  font-weight: 300;
  text-transform: capitalize;
  color: rgba(12, 17, 43, 0.9);
  margin-bottom: 18px;
`;

const _List = ({ state }) => {
  // Get the data of the current list.
  const data = state.source.get(state.router.link);

  return (
    <Container>
      {/* If the list is a taxonomy, we render a title. */}
      {data.isTaxonomy && (
        <Header>
          {data.taxonomy}:{" "}
          <b>{decode(state.source[data.taxonomy][data.id].name)}</b>
        </Header>
      )}

      {/* If the list is for a specific author, we render a title. */}
      {data.isAuthor && (
        <Header>
          Author: <b>{decode(state.source.author[data.id].name)}</b>
        </Header>
      )}

      {/* Iterate over the items of the list. */}
      <ItemWrapper>
        {data.items.map(({ type, id }) => {
          const item = state.source[type][id];
          // Render one Item component for each one.
          return <Item key={item.id} item={item} />;
        })}
      </ItemWrapper>
    </Container>
  );
};

const List = connect(_List);

const Wrapper = styled.div`
  display: grid;
  gap: 20px;
  width: 100%;
  padding: 24px 64px;
  @media (max-width: 768px) {
    padding: 12px;
  }
  grid-template-areas:
    "content"
    "sidebar"
    "pagination";
  @media (min-width: 768px) {
    grid-template-columns: 4fr 1fr;
    grid-template-areas:
      "content sidebar"
      "pagination pagination";
  }
`;

const StyledList = styled(List)`
  grid-area: content;
`;
const StyledPagination = styled(Pagination)`
  grid-area: pagination;
`;
const StyledSidebar = styled(Sidebar)`
  grid-area: sidebar;
`;

const ListPage = ({ state }) => {
  return (
    <Wrapper>
      <StyledList />
      <StyledSidebar />
      <StyledPagination />
    </Wrapper>
  );
};

export default connect(ListPage);
