import React from "react";
import Link from "../link";
import { connect, styled, css, keyframes } from "frontity";
import FeaturedMedia from "../featured-media";

const shadowPulse = keyframes` 
{
  0% {
    box-shadow: 0 0 0 0px rgba(0,168,172,.4);
  }
  100% {
    box-shadow: 0 0 0 12px rgba(0, 0, 0, 0);
  }
}`;

const StyledCard = styled.div`
  position: relative;
  min-width: 100%;
  min-height: 248px;
  margin-top: 90px;
  background-color: white;
  border-radius: 16px;
  box-shadow: rgb(203 210 214) 10px 10px 20px 10px;
  transform: rotate(0deg);
  flex: 0 1 60%;
  position: relative;
  /* box-shadow: rgb(203 210 214) 10px 10px 20px -10px;  */
`;

const Title = styled.div(() => {
  return css`
    font-size: 24px;
    font-weight: 700;
  `;
});

const Excerpt = styled.div(() => {
  return css`
    padding-left: 16px;
    font-weight: 400;
    border: none;
    font-size: 18px;
    text-align: right;
    margin-bottom: 10px;
    @media (max-width: 900px) {
      max-height: 170px;
      padding-left: 0px;
      margin-bottom: 10px;
    }
  `;
});

const ReadMore = styled.div`
  font-size: 16px;
  color: #00a8b6;
  font-weight: 600;
  border: 1px solid #00a8b6;
  border-radius: 16px;
  padding: 10px 18px;
  width: 172px;
  margin: 0px 16px 16px;
  animation: ${shadowPulse} 1.4s infinite;
  cursor: pointer;
  float: right;
`;
const CardBody = styled.div(() => {
  return css`
    font-size: 24px;
    padding: 16px 24px;
    text-align: left;
    margin-top: 68px;
    transition: all 500ms ease-in-out;
    @media (max-width: 900px) {
      margin-top: 80px;
    }
  `;
});

const Category = styled.div(() => {
  return css`
    background-color: #1b193b;
    /* background-color: rgba(23,193,124,1); */
    padding: 6px 10px;
    height: 34px;
    font-weight: 400;
    color: white;
    margin: 0 8px;
    border-bottom-left-radius: 10px;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 4px;
    border-top-left-radius: 4px;
    box-shadow: #1b193b 10px 10px 20px -10px;
  `;
});

const Categories = styled.div(() => {
  return css`
    position: absolute;
    display: flex;
    top: -20px;
    right: 20px;
  `;
});

const Wrapper = styled.div`
  max-width: 300px;
`;
const CustomCard = ({ item }) => {
  // const [analytics, setAnalytics] = useState({
  //     impressions: 127,
  //     comments: 35,
  // });

  return (
    <StyledCard>
      <Link link={item.link}>
        <CardBody>
          <Title dangerouslySetInnerHTML={{ __html: item.title.rendered }} />
          <Excerpt
            dangerouslySetInnerHTML={{
              __html: item.excerpt.rendered.slice(0, 160).concat(" [...]"),
            }}
          />
        </CardBody>
        {/* <Analytics
                onChange={(obj) =>
                    setAnalytics((prev) => ({
                        ...prev,
                        ...obj,
                    }))
                }
                comments={analytics.comments}
                impressions={analytics.impressions}
            /> */}

        {/* <Categories>
                    {card.categories.map((category) => (
                        <Category key={category.id}>{category.name}</Category>
                    ))}
                </Categories> */}

        <FeaturedMedia mode={"creative"} id={item.featured_media} />
      </Link>
      <Link link={item.link}>
        <ReadMore>{"Continua a leggere 😀"}</ReadMore>
      </Link>
    </StyledCard>
  );
};

export default connect(CustomCard);
