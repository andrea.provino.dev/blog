import React, { useEffect } from "react";


const Sitemap = () => {
    useEffect(() => {
        window.location.href = 'https://andreaprovino.it/sitemap.xml'
    }, [])
    return <p>Sitemap</p>
}


export default Sitemap