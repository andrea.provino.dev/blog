const settings = {
  name: "blog",
  state: {
    frontity: {
      url: "https://andreaprovino.it",
      title: "Machine Learning and Data Science Blog",
      description:
        "Blog italiano intelligenza artificiale, machine learning, data science e privacy enhancing technologies",
    },
  },
  packages: [
    {
      name: "@frontity/mars-theme",
      state: {
        theme: {
          version: "0.0.6",
          menu: [
            ["🏡 Home", "/"],
            ["⚖️ GDPR", "/category/gdpr-compliant/"],
            ["🧪 Data Science", "/category/data-science/"],
            ["🧠 Machine Learning", "/category/machine-learning/"],
            ["🔐 Privacy", "/category/privacy-preserving/"],
          ],
          featured: {
            showOnList: true,
            showOnPost: true,
          },
        },
      },
    },
    {
      name: "@frontity/wp-source",
      state: {
        source: {
          url: "https://andreaprovino.it",
        },
      },
    },
    "@frontity/tiny-router",
    "@frontity/html2react",
    "@frontity/head-tags",
  ],
};

export default settings;
